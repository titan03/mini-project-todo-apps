require('dotenv').config();
const express = require('express');
const app = express()
const logger = require('morgan');
const cors = require('cors');
const Router = require('./router');

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))
app.use(logger('dev'))
app.use(cors())

app.use(Router)

//! Not Found Error
app.get('*', (req, res) => {
    res.status(404).send('<h1> RTFM </h1>')
})

module.exports = app