const jwt = require('jsonwebtoken');
const { User } = require('../models');

const Authenticate = async (req, res, next) => {
    try {
        let token = req.headers.authorization
        let payload = await jwt.verify(token, process.env.JWT_SECRET)
        req.user = await User.findByPk(payload.id)
        next()
    } catch (error) {
        res.status(403).json({
            status: false,
            message: [error.message]
        })
    }
}

module.exports = Authenticate