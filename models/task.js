'use strict';
module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.Sequelize.Model
  class Task extends Model { }

  Task.init({
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input value title`
        }
      }
    },
    description: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input value description`
        }
      }
    },
    importance: {
      type: DataTypes.BOOLEAN,
      validate: {
        notEmpty: true,
        isIn: [[true, false]]
      }
    },
    completion: {
      type: DataTypes.BOOLEAN,
      validate: {
        notEmpty: true,
        isIn: [[true, false]]
      }
    },
    due_date: {
      type: DataTypes.DATE,
      validate: {
        customValidator(value) {
          let date = new Date()
          if (value < date.toISOString().split('T')[0]) {
            throw new Error(`The Date must be Bigger or Equal Today Date`)
          }
        },
        isDate: true
      }
    },
    UserId: DataTypes.INTEGER
  }, { sequelize })
  // const Task = sequelize.define('Task', {

  // }, {});
  Task.associate = function (models) {
    // associations can be defined here
    Task.belongsTo(models.User, { foreignKey: 'UserId' })
  };
  return Task;
};