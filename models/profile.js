'use strict';
module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.Sequelize.Model
  class Profile extends Model { }

  Profile.init({
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input your name`
        }
      }
    },
    picture: {
      type: DataTypes.TEXT,
      defaultValue: 'https://ik.imagekit.io/ckb21lc9cd/default-profile-picture1-744x744_QpfXO9KtJ.jpg',
      validate: {
        notEmpty: {
          msg: `Please input your picture`
        },
        isUrl: true
      }
    },
    UserId: DataTypes.INTEGER
  }, { sequelize })
  // const Profile = sequelize.define('Profile', {

  // }, {});
  Profile.associate = function (models) {
    // associations can be defined here
    Profile.belongsTo(models.User, { foreignKey: 'UserId' })
  };
  return Profile;
};