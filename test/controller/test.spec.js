const Profile = require('../../controller/Profile');
const faker = require('faker')



describe('test.updateProfile / upload picture', () => {
    //!factory function
    function mockRequest() {
        return {}
    }

    //!factory function
    function mockResponse() {
        const res = {}
        res.status = jest.fn().mockReturnValue(res)
        res.json = jest.fn().mockReturnValue(res)
        return res
    }

    test('Should alter the res object, and call status method with 202 in it', async () => {
        // Setup
        const req = mockRequest();
        const res = mockResponse();

        req.file = {};
        req.file.buffer = Buffer.alloc(10);
        req.file.originalname = faker.system.fileName()
        // End of setup
        console.log(req.file.buffer, '==================ini buffer===================');
        console.log(req.file.originalname, '=============ini original name====================');
        console.log('=================req file====================== ', req.file);

        // Do the test
        await Profile.updateProfile(req, res);

        // Expect
        // expect(res.status).toBeCalledWith(200);
        // expect(res.json).toBeCalledWith(
        //     expect.objectContaining({
        //         status: 'Success'
        //     })
        // )
    })

})