const request = require('supertest');
const db = require('../models');
// const { queryInterface } = sequelize
const app = require('../app');
// const bcrypt = require('bcryptjs')

describe('User API and Profile Collection especially Create Profile', () => {
    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    });

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    })

    // let tokenError = ''
    let token;
    // let UserId;
    describe('POST /api/v1/users/register', () => {
        test('Status code 201 should successfully create new user', (done) => {
            request(app)
                .post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'budi',
                    email: 'budi@gmail.com',
                    password: '123456',
                })
                .then((res) => {
                    token = res.body.token
                    // console.log(token, '===============ini token di user test=======================');
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('status');
                    expect(res.body).toHaveProperty('token');
                    done()
                }).catch(console.log);
        });

        test(`Status code 422 should can't create new user`, (done) => {
            request(app)
                .post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'darto',
                    email: '',
                    password: '123456'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log)
        })
    });

    // let token = null
    describe('POST /api/v1/users/login', () => {
        test('Status code 202 should successfully login', (done) => {
            request(app)
                .post('/api/v1/users/login')
                .set('Contest-type', 'application/json')
                .send({
                    email: 'budi@gmail.com',
                    password: '123456'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202)
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('status');
                    expect(res.body).toHaveProperty('token');
                    done()
                }).catch(console.log);
        });

        test(`Statuc code 400 should can't login`, (done) => {
            request(app)
                .post('/api/v1/users/login')
                .set('Contest-type', 'application/json')
                .send({
                    email: 'budi@gmail.com',
                    password: '123'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual(false)
                    expect(res.body.message).toEqual(`email or password invalid`)
                    done()
                }).catch(console.log);
        })

        test(`Status code 422 should can't login`, (done) => {
            request(app)
                .post('/api/v1/users/login')
                .set('Contest-type', 'application/json')
                .send({
                    email: null,
                    password: null
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log);
        })
    });

    describe('GET /api/v1/users/', () => {
        test('Status code 200 should successfully get one data user', (done) => {
            request(app)
                .get('/api/v1/users/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('status');
                    expect(res.body).toHaveProperty('user');
                    done()
                }).catch(console.log);
        });
    });

    describe('GET /api/v1/users/all', () => {
        test('Status code 200 should successfully get one data user', (done) => {
            request(app)
                .get('/api/v1/users/all')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('status');
                    expect(res.body).toHaveProperty('user');
                    done()
                }).catch(console.log);
        });
    });

    describe('PUT /api/v1/users/', () => {
        test('Status code 202 should successfully update data User', (done) => {
            request(app)
                .put('/api/v1/users/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    email: 'test101@mail.com',
                    name: 'test101'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202)
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log);
        });

        test('Status code 422 should successfully update data User', (done) => {
            request(app)
                .put('/api/v1/users/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    email: 'test101.com',
                    name: 'test101'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log);
        });
    });
});