const request = require('supertest');
const db = require('../models');
const app = require('../app');

describe('Profile API Collection', () => {
    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    });

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    })

    let token
    describe('POST /api/v1/users/register', () => {
        test('Status code 201 should successfully create new user', (done) => {
            request(app)
                .post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'test2',
                    email: 'test2@mail.com',
                    password: '123456'
                })
                .then((res) => {
                    token = res.body.token
                    // console.log(token, '===================ini token di todo test===================');
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('status');
                    expect(res.body).toHaveProperty('token');
                    done()
                }).catch(console.log);
        });

        test(`Status code 422 should can't create new user`, (done) => {
            request(app)
                .post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'j@gmail.com',
                    password: ''
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log)
        })
    });

    describe('PUT /api/v1/profiles/', () => {
        test('Status code 202 should successfully update data Profile', (done) => {
            request(app)
                .put('/api/v1/profiles/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .attach('picture', 'image/anonim.jpg')
                .field({
                    name: 'titan'
                })
                .then((res) => {
                    console.log('res.body :>> ', res.body)
                    // expect(imagekit.prototype.upload.mock.calls.length).toBe(1)
                    expect(res.statusCode).toBe(202)
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log);
        });

        test(`Status code 422 should can't update data Profile`, (done) => {
            request(app)
                .put('/api/v1/profiles/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .attach('picture', '')
                .field({
                    name: ''
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log);
        });
    });

    describe('GET /api/v1/profiles/all', () => {
        test('Status code 200 should successfully get all data Profile', (done) => {
            request(app)
                .get('/api/v1/profiles/all')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('data')
                    done()
                }).catch(console.log);
        });
    });

    describe('GET /api/v1/profiles/', () => {
        test('Status code 200 should successfully get one data Profile', (done) => {
            request(app)
                .get('/api/v1/profiles/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('profile')
                    done()
                }).catch(console.log);
        });
    });
});