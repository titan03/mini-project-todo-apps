const request = require('supertest');
const db = require('../models');
// const { queryInterface } = sequelize
const app = require('../app');
// const Faker = require('../lib/faker');


describe('Todos API Collection', () => {

    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    });

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    })

    let token;
    // let tokenError = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwiZW1haWwiOiJtYnV0QG1idXQuY29tIiwiaWF0IjoxNTkwOTE2NjY5fQ.fVQL5Qsnk-ROdl1A72b--nWb4iZGXK-8JRBOeqg8-Bk';
    // let UserId;
    describe('POST /api/v1/users/register', () => {
        test('Status code 201 should successfully create new user', (done) => {
            request(app)
                .post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'test',
                    email: 'test@mail.com',
                    password: '123456'
                })
                .then((res) => {
                    token = res.body.token
                    // console.log(token, '===================ini token di todo test===================');
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('status');
                    expect(res.body).toHaveProperty('token');
                    done()
                }).catch(console.log);
        });

        test(`Status code 422 should can't create new user`, (done) => {
            request(app)
                .post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'j@gmail.com',
                    password: null
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log)
        })
    });

    describe('POST /api/v1/todos', () => {
        test('Status code 201 should successfully create new Todo', (done) => {
            request(app)
                .post('/api/v1/todos/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    title: 'Learn deno and node',
                    description: 'Learn Mode On',
                    due_date: '2020-05-28'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(201);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('todo')
                    // UserId = res.body.todo.UserId
                    done()
                }).catch(console.log);
        });

        test('Status code 422 should not create new Todo', (done) => {
            request(app)
                .post('/api/v1/todos/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    title: null,
                    description: null,
                    due_date: '2020-05-28'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log);
        });
    });


    describe('GET /api/v1/todos', () => {
        test('Status code 200 should successfully get Todos', (done) => {
            request(app)
                .get('/api/v1/todos')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('todos')
                    done()
                }).catch(console.log);
        });
    });


    describe('GET /api/v1/todos/completion', () => {
        test('Status code 200 should succesfully get completion Todos', (done) => {
            request(app)
                .get('/api/v1/todos/completion')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('todos')
                    done()
                }).catch(console.log);
        });
    });



    describe('GET /api/v1/todos/importance', () => {
        test('Status code 200 should succesfully get importance Todos', (done) => {
            request(app)
                .get('/api/v1/todos/importance')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('todos')
                    done()
                }).catch(console.log);
        });
    });


    describe('PUT /api/v1/todos/:id', () => {
        test('Status code 202 should successfully update Todo ', (done) => {
            request(app)
                .put('/api/v1/todos/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    completion: true,
                    importance: true
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log)
        });

        test(`Status code 422 should can't update Todo`, (done) => {
            request(app)
                .put('/api/v1/todos/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    completion: null,
                    importance: null
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual(false)
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log)
        });
    });


    describe('DELETE /api/v1/todos/:id', () => {
        test('Status code 200 should successfully delete Todo`', (done) => {
            request(app)
                .delete('/api/v1/todos/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('message')
                    done()
                }).catch(console.log)
        });
    });
});