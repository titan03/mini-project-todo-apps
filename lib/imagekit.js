const ImageKit = require("imagekit");

let imagekit = new ImageKit({
    publicKey: process.env.PUBLIC_KEY_IMAGEKIT,
    privateKey: process.env.PRIVATE_KEY_IMAGEKIT,
    urlEndpoint: process.env.URLENDPOINT_IMAGEKIT
});

module.exports = imagekit