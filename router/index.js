const router = require('express').Router();
const User = require('./User');
const Todo = require('./Todo');
const Profile = require('./Profile');

router.use('/api/v1/todos', Todo)
router.use('/api/v1/users', User)


// !update profile here
router.use('/api/v1/profiles', Profile)

module.exports = router