const router = require('express').Router();
const User = require('../controller/User');
const Auth = require('../middleware/Auth');

router.post('/register', User.register) //! Register
router.post('/login', User.login) //! Login

router.put('/', Auth, User.updateUser) //! not use ID

router.get('/all', Auth, User.getUser) //! get all user
router.get('/', Auth, User.getOneUser) //! get one user

// router.get('/verify', Auth, User.sendMail)

module.exports = router