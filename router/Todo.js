const router = require('express').Router();
const Todo = require('../controller/Todo');
const Auth = require('../middleware/Auth');
const CheckOwner = require('../middleware/CheckOwner');

//! CREATE TODO
router.post('/', Auth, Todo.create)

//! UPDATE TODO by ID
router.put('/:id', Auth, CheckOwner('Task'), Todo.update)

//! DELETE TODO by ID
router.delete('/:id', Auth, CheckOwner('Task'), Todo.delete)

//! GET ALL TODO OR SHOW ALL TODO
router.get('/', Auth, Todo.getAll)

//! GET ALL TODO by IMPORTANCE
router.get('/importance', Auth, Todo.getImportance)

//! GET ALL TODO by COMPLETION
router.get('/completion', Auth, Todo.getCompletion)


module.exports = router