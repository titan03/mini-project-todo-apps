const router = require('express').Router();
const Profile = require('../controller/Profile');
const uploader = require('../middleware/uploader');
const Auth = require('../middleware/Auth');

// router.post('/', uploader.single('image'), Profile.index);
router.get('/all', Auth, Profile.getProfile) //! get all data profile
router.get('/', Auth, Profile.getOneProfile) //! get one data profile

//! update profile
router.put('/', Auth, uploader.single('picture'), Profile.updateProfile) //! not use ID

module.exports = router