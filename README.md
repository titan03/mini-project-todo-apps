# `Mini Project Todo Apps`



# `TODO App API`

An Simple Todo Application with some features:
1. Get All Todo by completion
2. Get all Todo
3. Get All Todo by Importance
4. Create Todo
5. Update Todo
6. Delete Todo
7. Sign In
8. Sign Up
9. Get one User
10. Get all User
11. Update User
12. Get one Profile
13. Update Profile
14. Get all Profile
### Documentation API in POSTMAN

```
    https://documenter.getpostman.com/view/10887927/SztA9AC4
```

# Server


### **`Todos Collection / List All Todo by Completion / Get All Todo by Completion`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/todos/completion

_Method_
```
    GET
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
   {
    "status": "Success",
    "todos": {
        "count": 18,
        "rows": [
            {
                "id": 62,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:52.892Z",
                "updatedAt": "2020-06-03T20:15:52.892Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 63,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:57.612Z",
                "updatedAt": "2020-06-03T20:15:57.612Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 64,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:58.708Z",
                "updatedAt": "2020-06-03T20:15:58.708Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 65,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:59.152Z",
                "updatedAt": "2020-06-03T20:15:59.152Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 66,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:59.725Z",
                "updatedAt": "2020-06-03T20:15:59.725Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 67,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:00.362Z",
                "updatedAt": "2020-06-03T20:16:00.362Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 68,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:01.138Z",
                "updatedAt": "2020-06-03T20:16:01.138Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 69,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:01.639Z",
                "updatedAt": "2020-06-03T20:16:01.639Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 70,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:02.295Z",
                "updatedAt": "2020-06-03T20:16:02.295Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 71,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": true,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:02.975Z",
                "updatedAt": "2020-06-03T20:16:02.975Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            }
        ]
    }
}
```


### **`Todos Collection / List All Todo by Importance / Get All Todo by Importance`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/todos/importance

_Method_
```
    GET
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
    {
    "status": "Success",
    "todos": {
        "count": 18,
        "rows": [
            {
                "id": 62,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:52.892Z",
                "updatedAt": "2020-06-03T20:15:52.892Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 63,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:57.612Z",
                "updatedAt": "2020-06-03T20:15:57.612Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 64,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:58.708Z",
                "updatedAt": "2020-06-03T20:15:58.708Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 65,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:59.152Z",
                "updatedAt": "2020-06-03T20:15:59.152Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 66,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:59.725Z",
                "updatedAt": "2020-06-03T20:15:59.725Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 67,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:00.362Z",
                "updatedAt": "2020-06-03T20:16:00.362Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 68,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:01.138Z",
                "updatedAt": "2020-06-03T20:16:01.138Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 69,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:01.639Z",
                "updatedAt": "2020-06-03T20:16:01.639Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 70,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:02.295Z",
                "updatedAt": "2020-06-03T20:16:02.295Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 71,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": true,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:02.975Z",
                "updatedAt": "2020-06-03T20:16:02.975Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            }
        ]
    }
}
```


### **`Todos Collection / List All Todo / Get All Todo`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/todos/

_Method_
```
    GET
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
   {
    "status": "Success",
    "todos": {
        "count": 18,
        "rows": [
            {
                "id": 62,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:52.892Z",
                "updatedAt": "2020-06-03T20:15:52.892Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 63,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:57.612Z",
                "updatedAt": "2020-06-03T20:15:57.612Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 64,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:58.708Z",
                "updatedAt": "2020-06-03T20:15:58.708Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 65,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:59.152Z",
                "updatedAt": "2020-06-03T20:15:59.152Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 66,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:15:59.725Z",
                "updatedAt": "2020-06-03T20:15:59.725Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 67,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:00.362Z",
                "updatedAt": "2020-06-03T20:16:00.362Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 68,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:01.138Z",
                "updatedAt": "2020-06-03T20:16:01.138Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 69,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:01.639Z",
                "updatedAt": "2020-06-03T20:16:01.639Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 70,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:02.295Z",
                "updatedAt": "2020-06-03T20:16:02.295Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            },
            {
                "id": 71,
                "title": "adaadaada",
                "description": "adaadaada",
                "importance": false,
                "completion": false,
                "due_date": "2020-03-03T00:00:00.000Z",
                "UserId": 16,
                "createdAt": "2020-06-03T20:16:02.975Z",
                "updatedAt": "2020-06-03T20:16:02.975Z",
                "User": {
                    "id": 16,
                    "email": "test12@mail.com",
                    "password": "$2a$10$j6Qds2r75h7HUE8JGITjUOVs/9kl6DGeh0VW6vQN20/kjkdHicsly",
                    "createdAt": "2020-06-03T20:14:58.667Z",
                    "updatedAt": "2020-06-03T20:14:58.667Z"
                }
            }
        ]
    }
}
```


### **`Todos Collection / Create a New Todo`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/todos/

_Method_
```
    POST
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    {
        "title": "learn unit test",
        "description": "jest etc..",
        "importance": false,
        "completion": false,
        "due_date": "2020-05-26",
    }
```

_Response (201)_ | Created
```
    {
        "id": 1,
        "title": "learn unit test",
        "description": "jest etc..",
        "importance": false,
        "completion": false,
        "due_date": "2020-05-26T00:00:00.000Z",
        "UserId": 1,
        "updatedAt": "2020-05-26T19:05:27.399Z",
        "createdAt": "2020-05-26T19:05:27.399Z"
    }
```


### **`Todos Collection / Edit Todo by Id / Update Todo By Id`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/todos/:id

_Method_
```
    PUT
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    {
        "title": "learn unit test",
        "description": "jest etc..",
        "importance": true,
        "completion": true,
        "due_date": "2020-05-26",
    }
```

_Response (202)_ | Accepted
```
    {
        "status": "Success",
        "message": "Todo with id 1 has been updated"
    }
```


### **`Todos Collection / Delete Todo by Id`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/todos/:id

_Method_
```
    DELETE
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
    {
    "status": "Success",
    "message": "Todo with id 3 has been deleted"
    }
```


### **`User Register / Register new User / Create User Profile`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/users/register

_Method_
```
    POST
```

_Request Header_
```
    Empty
```

_Request Body_
```
    {
        "name" : < Name user here >
        "email": < Email user here >
        "password": < Password User here >
    }
```

_Response (201)_ | Created
```
    {
        "status": "Success",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJqb2huZG9lQG1haWwuY29tIiwiaWF0IjoxNTkwNTE4OTQwfQ.21eBzxPWP5O1Q-EAXRvOCaLNwk01o7cV67JcQBR2wzE"
    }
```


### **`User Login / Sign in User`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/users/login

_Method_
```
    POST
```

_Request Header_
```
    Empty
```

Request Body_
```
    {
        "email": < Email user here >
        "password": < Password User here >
    }
```

_Response (202)_ | Accepted
```
    {
        "status": "Success",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJqb2huZG9lQG1haWwuY29tIiwiaWF0IjoxNTkwNTE5Mjg5fQ.lLfjmyu8YHOlatncE0n5wTXTNHGY9SRmpd9Se7iQ47A"
    }
```

_Response (400)_ | Bad Request < Error >
```
    {
        "status": false,
        "message": "email or password invalid"
    }
```


### **`User Collection / Get one User`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/users/

_Method_
```
    GET
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
   {
        "status": "Success",
        "user": {
            "id": 55,
            "email": "john@mail.com",
            "password": "$2a$10$JxSWX.eHqJVoO4.EL.Uv7.0mz5JLR.bmCpOLohSjSSzvfKE4RJhui",
            "createdAt": "2020-06-02T10:11:41.756Z",
            "updatedAt": "2020-06-02T10:11:41.756Z"
        }
    }
```


### **`User Collection / Get All User`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/users/all


_Method_
```
    GET
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
    {
        "status": "Success",
        "user": [
            {
                "id": 1,
                "email": "bambang@mail.com",
                "password": "$2a$10$flbThjf2SaFtuivIbB0wbemGuijxRl3xeFyFxpifQl2OEShuBtRU.",
                "createdAt": "2020-05-29T15:35:49.076Z",
                "updatedAt": "2020-05-29T15:35:49.076Z"
            },
            {
                "id": 2,
                "email": "farizrizkyrizal@gmail.com",
                "password": "$2a$10$jlzjJmGIno1AUPf7IM.NLeHdrSsLoa3sDEuFdbHfzFMYeThxCG/7K",
                "createdAt": "2020-05-31T14:54:56.056Z",
                "updatedAt": "2020-05-31T14:54:56.056Z"
            },
            {
                "id": 3,
                "email": "yogie@mail.com",
                "password": "$2a$10$fLBwHZ4jSxIPie4SM8XvW.5lb0MmirLULBIqgpVnQ37caxliLe6Pq",
                "createdAt": "2020-06-02T03:46:28.599Z",
                "updatedAt": "2020-06-02T03:46:28.599Z"
            },
            {
                "id": 11,
                "email": "adu@mail.com",
                "password": "$2a$10$iFW7.5yPxCkCuNwfC3quYO5H.K7E03O8HpxURGZe/QoXHCZVwZega",
                "createdAt": "2020-06-02T04:11:23.456Z",
                "updatedAt": "2020-06-02T04:11:23.456Z"
            },
            {
                "id": 13,
                "email": "yudista@mail.com",
                "password": "$2a$10$aNDUfP.fzocnj.pHwsHaiuXyZm/YjQxQ4p/a9ajJWwGmdjhv6tt9G",
                "createdAt": "2020-06-02T04:40:58.052Z",
                "updatedAt": "2020-06-02T04:40:58.052Z"
            }
        ]
    }
```


### **`User Collection / Edit User by Id / Update User`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/users/

_Method_
```
    PUT
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
{
    "email": < Email user here >
    "password": < Password User here >
}
```

_Response (202)_ | Accepted
```
{
    "status": "Success",
    "message": "User with id: 19 has been updated"
}
```



### **`Profile Collection / Update Profile`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/profiles/

_Method_
```
    PUT
```

_Request Header_
```
    Authorization: < User token is here >
```

_Request Body_
```
{
    name: 'John Doe',
    picture: < upload your profile picture here >
}
```

_Response (202)_ | Accepted
```
{
    "status": "Success",
    "message": "Profile with id: 1 has been updated"
}
```



### **`Profile Collection / Get All profile`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/profiles/all


_Method_
```
    GET
```

_Request Header_
```
{
    Authorization: < User token is here >
}
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
    {
        "status": "Success",
        "data": {
            "profile": [
                {
                    "id": 1,
                    "name": "sugianto",
                    "picture": "https://ik.imagekit.io/ckb21lc9cd/IMG-1590845629048_XvZk-XudY.jpg",
                    "UserId": 1,
                    "createdAt": "2020-05-29T15:35:49.307Z",
                    "updatedAt": "2020-05-30T13:33:51.016Z"
                },
                {
                    "id": 2,
                    "name": "Fariz rizky",
                    "picture": "https://ik.imagekit.io/ckb21lc9cd/default-profile-picture1-744x744_QpfXO9KtJ.jpg",
                    "UserId": 2,
                    "createdAt": "2020-05-31T14:54:56.566Z",
                    "updatedAt": "2020-05-31T14:54:56.566Z"
                },
                {
                    "id": 3,
                    "name": "yogie",
                    "picture": "https://ik.imagekit.io/ckb21lc9cd/default-profile-picture1-744x744_QpfXO9KtJ.jpg",
                    "UserId": 3,
                    "createdAt": "2020-06-02T03:46:28.812Z",
                    "updatedAt": "2020-06-02T03:46:28.812Z"
                },
                {
                    "id": 4,
                    "name": "adu",
                    "picture": "https://ik.imagekit.io/ckb21lc9cd/default-profile-picture1-744x744_QpfXO9KtJ.jpg",
                    "UserId": 11,
                    "createdAt": "2020-06-02T04:11:23.620Z",
                    "updatedAt": "2020-06-02T04:11:23.620Z"
                },
                {
                    "id": 5,
                    "name": "yudista",
                    "picture": "https://ik.imagekit.io/ckb21lc9cd/default-profile-picture1-744x744_QpfXO9KtJ.jpg",
                    "UserId": 13,
                    "createdAt": "2020-06-02T04:40:58.183Z",
                    "updatedAt": "2020-06-02T04:40:58.183Z"
                }
            ]
        }
    }
```


### **`Profile Collection / Get one Profile`**

#### Routes / Link: https://titan-todoapp.herokuapp.com/api/v1/profiles/

_Method_
```
    GET
```

_Request Header_
```
    {
        Authorization: < User token is here >
    }
```

_Request Body_
```
    Empty
```

_Response (200)_ | Ok
```
   {
        "status": "Success",
        "profile": {
            "id": 15,
            "name": "john",
            "picture": "https://ik.imagekit.io/ckb21lc9cd/default-profile-picture1-744x744_QpfXO9KtJ.jpg",
            "UserId": 55,
            "createdAt": "2020-06-02T10:11:41.909Z",
            "updatedAt": "2020-06-02T10:11:41.909Z"
        }
    }
```