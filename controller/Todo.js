const {
    Task,
    User
} = require('../models');

class Tasks {
    static async create(req, res) {
        const {
            title,
            description,
            importance,
            completion,
            due_date
        } = req.body

        let todo = await Task.create({
            title,
            description,
            importance,
            completion,
            due_date,
            UserId: req.user.id
        }).catch((error) => {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        })

        if (todo) {
            res.status(201).json({
                status: 'Success',
                todo
            })
        }
    }

    static async getAll(req, res) {
        const query = req.query
        try {
            let todos = await Task.findAndCountAll({
                where: {
                    UserId: req.user.id
                },
                include: [{
                    model: User
                }],
                limit: query.limit <= 10 ? query.limit : 10,
                offset: query.offset,
                order: [
                    ['due_date', 'ASC']
                ]
            })
            res.status(200).json({
                status: 'Success',
                todos
            })
        } catch (error) {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }

    static async getCompletion(req, res) {
        /**
        let limit = 10;   // number of records per page
        let offset = 0;
        Task.findAndCountAll()
            .then((data) => {
                let page = req.params.page;      // page number
                // let pages = Math.ceil(data.count / limit);
                offset = limit * (page - 1);
                Task.findAll({
                    where: { completion: true },
                    limit: limit,
                    offset: offset
                })
                    .then((users) => {
                        res.status(200).json({
                            count: data.count,
                            // pages: pages,
                            result: users,
                        });
                    });
            })
            .catch(function (error) {
                res.status(422).json({
                    status: false,
                    message: [error.message]
                })
            });
         */

        const query = req.query
        try {
            let todos = await Task.findAndCountAll({
                where: {
                    UserId: req.user.id,
                    completion: true
                },
                include: [{
                    model: User
                }],
                limit: query.limit <= 10 ? query.limit : 10,
                offset: query.offset,
                order: [
                    ['due_date', 'ASC']
                ]
            })
            res.status(200).json({
                status: 'Success',
                todos
            })
        } catch (error) {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }

    static async getImportance(req, res) {
        const query = req.query
        try {
            let todos = await Task.findAndCountAll({
                where: {
                    UserId: req.user.id,
                    importance: true
                },
                include: [{
                    model: User
                }],
                limit: query.limit <= 10 ? query.limit : 10,
                offset: query.offset,
                order: [
                    ['due_date', 'ASC']
                ]
            })
            res.status(200).json({
                status: 'Success',
                todos
            })
        } catch (error) {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }

    static async update(req, res) {
        const {
            title,
            description,
            importance,
            completion,
            due_date
        } = req.body
        const id = req.params.id
        try {
            await Task.update({
                title,
                description,
                importance,
                completion,
                due_date
            }, {
                where: {
                    id
                }
            })
            // console.log(user, '==============ini user===================update');
            res.status(202).json({
                status: 'Success',
                message: `Todo with id ${id} has been updated`
            })
        } catch (error) {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }

    static async delete(req, res) {
        const id = req.params.id
        try {
            await Task.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({
                status: 'Success',
                message: `Todo with id ${id} has been deleted`
            })
        } catch (error) {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }
}

module.exports = Tasks