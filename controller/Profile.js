const imagekit = require('../lib/imagekit');
const {
    Profile
} = require('../models');

class Profiles {

    /**
     *  static async index(req, res) {
        
        let filename = req.file.originalname.split('.');
        filename = `IMG-${Date.now()}.${filename[filename.length - 1]}`;

        fs.writeFileSync(
            path.resolve(__dirname, '..', 'uploads', filename),
            req.file.buffer
        );
        res.send('File uploaded!');
        res.send(req.file.url)
         
    }
     */

    static async updateProfile(req, res) {
        // console.log(' this is values:>> ', req.file);
        // res.end()
        try {
            // console.log(req.file, 'ini file');
            const validFormat = req.file.mimetype === 'image/png' || req.file.mimetype === 'image/jpg' || req.file.mimetype === 'image/jpeg' || req.file.mimetype === 'image/gif';
            if (!validFormat) {
                new Error('cannot uplod file')
            }
            const split = req.file.originalname.split('.')
            const ext = split[split.length - 1]
            let img = await imagekit.upload({
                file: req.file.buffer, //required
                fileName: `IMG-${Date.now()}.${ext}`, //required
            })
            // console.log(img, '=====================ini image di controller===================');

            const id = req.user.id
            let profile = await Profile.update({
                name: req.body.name,
                picture: img.url
            }, {
                where: {
                    UserId: id
                }
            })

            if (profile) {
                res.status(202).json({
                    status: 'Success',
                    message: `Profile with id: ${id} has been updated`
                })
            }
        } catch (error) {
            // console.log(error, '==================ini error=====================');
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }

    static async getProfile(req, res) {
        let profile = await Profile.findAll()
            .catch((err) => {
                res.status(422).json({
                    status: 'Fail',
                    message: [err.message]
                })
            })

        if (profile) {
            res.status(200).json({
                status: 'Success',
                data: {
                    profile
                }
            })
        }
    }

    static async getOneProfile(req, res) {
        let id = req.user.id
        let profile = await Profile.findOne({
                where: {
                    UserId: id
                }
            })
            .catch((err) => {
                res.status(422).json({
                    status: false,
                    message: [err.message]
                })
            })

        if (profile) {
            res.status(200).json({
                status: 'Success',
                profile
            })
        }
    }
}

module.exports = Profiles