const {
    User,
    Profile
} = require('../models');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

class Users {
    static async register(req, res) {
        const {
            name,
            picture
        } = req.body

        try {
            let user = await User.create(req.body)
            await Profile.create({
                name,
                picture,
                UserId: user.id
            })
            let token = await jwt.sign({
                id: user.id,
                email: user.email
            }, process.env.JWT_SECRET)


            const msg = {
                to: user.email,
                from: 'Miniprojectglintzbe1@gmail.com',
                subject: 'Sending with Twilio SendGrid is Fun',
                text: `this is your token ${token}`,
                html: '<strong>and easy to do anywhere, even with Node.js</strong>',
            };

            await sgMail.send(msg);

            res.status(201).json({
                status: 'Success',
                token
            })
        } catch (error) {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }

    static async login(req, res) {
        const {
            email,
            password
        } = req.body
        try {
            let user = await User.findOne({
                where: {
                    email: email.toLowerCase()
                }
            })
            if (user && bcrypt.compareSync(password, user.password)) {
                let token = jwt.sign({
                    id: user.id,
                    email: user.email
                }, process.env.JWT_SECRET)
                res.status(202).json({
                    status: 'Success',
                    token
                })
            } else {
                res.status(400).json({
                    status: false,
                    message: `email or password invalid`
                })
            }
        } catch (error) {
            res.status(422).json({
                status: false,
                message: [error.message]
            })
        }
    }

    static async updateUser(req, res) {
        let {
            email,
            password
        } = req.body
        if (password !== null || password !== '' || password !== undefined) {
            password = bcrypt.hashSync(String(password), 10)
        }
        let id = req.user.id
        let user = await User.update({
                email,
                password
            }, {
                where: {
                    id
                }
            })
            .catch((err) => {
                res.status(422).json({
                    status: false,
                    message: [err.message]
                })
            })

        if (user) {
            res.status(202).json({
                status: 'Success',
                message: `User with id: ${id} has been updated`
            })
        }
    }

    static async getOneUser(req, res) {
        let id = req.user.id
        let user = await User.findOne({
                where: {
                    id
                }
            })
            .catch((err) => {
                console.log(err);
                res.status(422).json({
                    status: false,
                    message: [err.message]
                })
            })

        if (user) {
            res.status(200).json({
                status: 'Success',
                user
            })
        }
    }

    static async getUser(req, res) {
        let user = await User.findAll()
        if (user)
            res.status(200).json({
                status: 'Success',
                user
            })
    }

    // static async sendMail(req, res) {
    //     sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    //     const msg = {
    //         to: 'titanioy98@gmail.com',
    //         from: 'titanioyudista98@gmail.com',
    //         subject: 'Sending with Twilio SendGrid is Fun',
    //         text: 'and easy to do anywhere, even with Node.js',
    //         // html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    //     };

    //     try {
    //         const result = await sgMail.send(msg);
    //         res.status(200).json({
    //             status: 'Success',
    //             result
    //         })
    //     } catch (error) {
    //         console.error(error);
    //         res.status(422).json({
    //             status: 'Failed',
    //             Error: [error.message]
    //         })
    //         if (error.response) {
    //             console.error(error.response.body)
    //         }
    //     }
    // }
}

module.exports = Users